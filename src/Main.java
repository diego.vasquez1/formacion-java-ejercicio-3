import empleados.EmpleadoContratista;
import empleados.EmpleadoRegular;
import empleados.EmpleadoTiempoParcial;
import empleados.EntidadEmpresa;
import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        LocalDate dateOld = LocalDate.of(2024, 2, 14); // Year, Month, Day
        LocalDate dateNew = LocalDate.of(2024, 2, 16);

        System.out.println(dateOld);
        System.out.println(dateNew);

        EntidadEmpresa empresa1 = new EntidadEmpresa("empresa 1");
        empresa1.addEmpleado(new EmpleadoRegular("123456789", "Juan Perez", 30, dateOld, -1500.0, 500.0));
        empresa1.addEmpleado(new EmpleadoRegular("44444", "Luis 2", 45, dateNew, 1300, 600.0));
        empresa1.addEmpleado(new EmpleadoContratista("555", "Luis 3", 45, dateOld, 1000));
        empresa1.addEmpleado(new EmpleadoContratista("6666", "Luis 4", 45, dateNew, 1010));
        empresa1.addEmpleado(new EmpleadoTiempoParcial("7777", "Luis 5", 45, dateOld, 10.0, 40));
        empresa1.addEmpleado(new EmpleadoTiempoParcial("8888", "Luis 6", 45, dateNew, 12.0, 60));

        System.out.println(empresa1);
        System.out.println(empresa1.getTotalSalaryCompany());

        LocalDate check1 = LocalDate.of(2024, 2, 1);
        LocalDate check2 = LocalDate.of(2024, 2, 29);

        System.out.println(empresa1.getEmployeesBetweenRange(check1, check2));
        System.out.println(empresa1.getSalaryMonthForAll());
    }
}
