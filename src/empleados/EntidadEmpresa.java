package empleados;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class EntidadEmpresa {
    private String nombre;
    private Map<String, Empleado> empleados;

    @Override
    public String toString() {
        return "tipos_empleados.Entidad_Empresa{" +
                "nombre='" + nombre + '\'' +
                ", empleados=" + empleados +
                '}';
    }

    public EntidadEmpresa(String nombre) {
        this.nombre = nombre;
        this.empleados = new HashMap<String, Empleado>();
    }

    public void addEmpleado(Empleado e){
        empleados.put(e.dni,e);
    }

    public double getTotalSalaryCompany(){
        return empleados.values().stream().mapToDouble(Empleado::calcular_salario_mensual).sum();
    }

    public List<Empleado> getEmployeesBetweenRange(LocalDate d1 , LocalDate d2){

        //we switch the variables so It most recent to the left 
        if(d1.isAfter(d2)){
            LocalDate temp = d2;
            d2= d1;
            d1= temp;
        }

        LocalDate finalD = d1;
        LocalDate finalD1 = d2;
        return  empleados.values().stream().filter(empleado -> empleado.is_between_dates(finalD, finalD1))
        .collect(Collectors.toList());
    }

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Map<String, Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(Map<String, Empleado> empleados) {
        this.empleados = empleados;
    }

    public List<String> getSalaryMonthForAll() {
        List<String> list_salary = new ArrayList();
        empleados.forEach((nombre,e)->list_salary.add(e.nombre+" = "+e.calcular_salario_mensual()+","));
        return list_salary;
    }
}
