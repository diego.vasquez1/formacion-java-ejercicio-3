package empleados;

import errores.SalarioNegativoException;

import java.time.LocalDate;
import java.util.Date;

public class EmpleadoRegular extends Empleado {
    double salario_base;
    double bonos_totales;

    public EmpleadoRegular(String dni, String nombre, int edad, LocalDate fecha_contratacion, double salario_base, double bonos_totales) {
        super(dni, nombre, edad, fecha_contratacion, ContractType.regular);
       try {
        if (salario_base < 0) {
            throw new SalarioNegativoException("Salary can't be less than 0");
        }
        this.salario_base = salario_base;
        this.bonos_totales = bonos_totales;
    } catch (SalarioNegativoException e) {
        // Handle the exception, print an error message, or take appropriate action
        System.err.println("Error: " + e.getMessage());
    }

    }

    @Override
    public double calcular_salario_mensual() {
        return salario_base+bonos_totales;
    }

    public void add_bono(double bono){
        this.bonos_totales += bono;
    }
}
