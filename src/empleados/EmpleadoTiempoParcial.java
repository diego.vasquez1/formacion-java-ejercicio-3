package empleados;

import java.time.LocalDate;
import java.util.Date;

public class EmpleadoTiempoParcial extends Empleado {
    private final double salario_por_hora;
    private int horas_trabajadas;

    public EmpleadoTiempoParcial(String dni, String nombre, int edad, LocalDate fecha_contratacion, double salario_por_hora, int horas_trabajadas) {
        super(dni, nombre, edad, fecha_contratacion, ContractType.tiempo_parcial);
        this.salario_por_hora = salario_por_hora;
        this.horas_trabajadas = horas_trabajadas;
    }


    @Override
    public double calcular_salario_mensual() {
        return salario_por_hora*horas_trabajadas;
    }

    public void aumentar_horas_trabajadas(int horas){
        horas_trabajadas +=horas;
    }
}
