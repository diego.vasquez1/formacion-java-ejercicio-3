package empleados;

import java.time.LocalDate;
import java.util.Date;

public class EmpleadoContratista extends Empleado {
    double salario_fijo;

    public EmpleadoContratista(String dni, String nombre, int edad, LocalDate fecha_contratacion, double salario_fijo) {
        super(dni, nombre, edad, fecha_contratacion, ContractType.contratisa);
        this.salario_fijo = salario_fijo;
    }

    @Override
    public double calcular_salario_mensual() {
        return salario_fijo;
    }
}
