package empleados;
import java.time.LocalDate;

public abstract class Empleado {

    String dni;
    String nombre;

    int edad;

    LocalDate fecha_contratacion;
    ContractType tipo_contrato;

    public Empleado(String dni, String nombre, int edad, LocalDate fecha_contratacion, ContractType tipo_contrato) {
        this.dni = dni;
        this.nombre = nombre;
        this.edad = edad;
        this.fecha_contratacion = fecha_contratacion;
        this.tipo_contrato = tipo_contrato;
    }

    public boolean is_between_dates(LocalDate f1 , LocalDate f2){
        return (fecha_contratacion.isAfter(f1) && fecha_contratacion.isBefore(f2));
    }
    public abstract double calcular_salario_mensual() ;

    @Override
    public String toString() {
        return "tipos_empleados.Empleado{" +
                "dni='" + dni + '\'' +
                ", nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", fecha_contratacion=" + fecha_contratacion +
                ", tipo_contrato=" + tipo_contrato +
                '}';
    }

}
